__Please leave this file un-modified__

## GIT Playground
As the name suggests, this repository is for testing out GIT. Feel free to practice GIT usage by cloning this repository and pushing new changes.

## Instructions
Assumptions about the audience, made in the creation of these instructions:
* Limited or no prior experience using GIT repositories or SSH key-based authentication.
* Windows as a primary OS
* Click-based repository usage is preferred (versus command-line).

If you want to use GitLab via the command line, we are assuming you know what you are doing or do not mind [teaching](https://git-scm.com/) [yourself](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html).

### Installing GIT on Windows
1. Download and install GIT for Windows, [here](https://git-scm.com/). The default is safe for all installation options.  
  a. If you are prompted for credentials during initial setup, _provide your full name and work email address_. The latter is especially important for associating your commits back to your GitLab user account.
2. Download and install TortoiseGIT for Windows, [here](https://tortoisegit.org/). The default is also safe for all installation options.  
  a. Again, if you are prompted for credentials during initial setup, _provide your full name and work email address_. The latter is especially important for associating your commits back to your GitLab user account.

### Installing SSH-Related Tools
1. Download and install [PuTTY](https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html). I suggest downloading and installing the full, 64-bit, MSI installer. Two of the programs it installs, PuTTYGen and Pageant (PuTTY Agent), are what we are after.

### Creating and Registering an SSH Key with GitLab
1. Run PuTTYgen. It should be as simple as going to the start menu and typing it in to find it.
2. Under parameters, select `ED25519` as the type of key to generate.
3. Click `Generate`. You will have to move the most a little to generate entropy/randomness before the key is created.
4. Once you see the public key and Key fingerprint filled in, optionally update the key comment with your work email address. The comment is entirely for your benefit when looking at the key later in GitLab settings.
5. Fill in a key passphrase. This is used to "unlock" the key for usage in the PuTTY Agent, and will have to be entered after every Windows log-in before GitLab repositories can be accessed.
6. Copy the public key to your clipboard.
7. In a browser, navigate to your user's [SSH Key Settings](https://gitlab.com/-/profile/keys).
8. Paste in the public key.
9. Click `Add Key`.
10. Return to PuttyGen and click `Save private key`. Save the file somewhere you will remember later.

### Loading Key into PuTTY Agent
This is the step that must be performed before accessing any remote GitLab repositories.  

1. Navigate to the key file you created in the previous step.
2. Double-click the key file. If prompted, select PuTTY Agent (pageant.exe) to open the file. It should alredy have a file association from the initial installation.
3. If you used a password when you created the key, enter it into the prompt.
4. You should now see a computer icon in the systray (by the clock) representing the Putty Authentication Agent.
5. You should now be able to clone and use repositories from GitLab. TortoiseGIT will automatically pick up on the Putty Agent session and use the key on your behalf.

### Testing it out
1. Navigate to a folder on your system where you would like to keep source code.
2. Right-click in the folder white-space and select `Git Clone...`  
![Git Clone menu item](uploads/image.png)
3. Paste in the clone URL for this repository `git@gitlab.com:cremmel_proship/git-playground.git`.  
![Clone repository dialog](uploads/image-1.png)
4. Click `OK`.
5. The content of the repository should successfully clone to a sub-folder.  
![Successful clone progression](uploads/image-2.png)  
![Cloned folder on disk](uploads/image-3.png)

## Making life easier with IDEs
While TortoiseGIT will automatically work with Putty Agent and SSH key remotes, IDEs (such as Visual Studio and VS Code) need one more step to work with this system.

Add a new Windows Environment Variable named `GIT_SSH` to the System and set it to the location of the PuTTY tool, "plink". It should be installed here on 64-bit systems: `C:\Program Files\PuTTY\plink.exe`.  
![image.png](./uploads/environment-variable-button.png)
